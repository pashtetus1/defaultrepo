﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CatFarmService.Helpers;
using CatFarmService.Models;
using CatFarmService.Models.Events;
using CatFarmService.Models.NlogDb;
using CatFarmService.Models.Telegram;
using CatFarmService.Models.Variables;
using CatFarmService.Works;
using CatFarmService.Works.DevicesWorks;
using CatFarmService.Works.Telegram;
using NLog;

namespace CatFarmService
{
    class MainWork
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger(); 
       
        public async void Start()
        {
            DatabaseInit();

            await TelegramProcedures.SetReservedUsers();
            await CheckNewComputerStartUp();
            

            RunAllWorks();
            _logger.Warn("Main work start");
        }

        private static void RunAllWorks()
        {
            var startingWorks = new List<BaseSafetyWork>
            {
                new DevicesMonitoring(),
                new DeviceHistoryWriting(),
                new TelegramBotWorker(),
                new TelegramNotificationSender(),
                new InternalWorks(),
                new WorkTimeWriter(),
                new DefferedAlertsWork()
            };
          
            foreach (var work in startingWorks)
            {
                Task.Factory.StartNew(work.Start);
            }
        }

        private static void DatabaseInit()
        {
            Directory.CreateDirectory(Tools.InternalDataDir);
            Directory.CreateDirectory(Tools.DbDir);
            ReserveDb.Initialize();
            NLogDb.Initialize();
            CatFarmDb.Initialize();
        }

        private async Task CheckNewComputerStartUp()
        {
            var lastStartTimeValue = await VariableProcedures.GetDateTimeVariable(CatVariables.ComputerStart);  
            var currentSystemUpTime = Tools.SystemUpTime();
            await VariableProcedures.SetVariable(CatVariables.ComputerStart, currentSystemUpTime);

            if (lastStartTimeValue == null || currentSystemUpTime.ToString() == lastStartTimeValue.ToString())
            {
                return;
            }
          
            var lastManualRestart = await VariableProcedures.GetDateTimeVariable(CatVariables.LastForceRestart);

            if (lastManualRestart != null && lastManualRestart.Value > lastStartTimeValue.Value)
            {
                await EventProcedures.SaveEvent(EventName.Expected, "Включение фермы");
                return;
            }

            await SaveComputerStartUpEvent(currentSystemUpTime);
        }

        private async Task SaveComputerStartUpEvent(DateTime systemUpTime)
        {
            var systemShutDownDate = await VariableProcedures.GetDateTimeVariable(CatVariables.LastWorkDate);
            var toolsDate = Tools.GetLastSystemShutdown();
            if (toolsDate > systemShutDownDate)
            {
                systemShutDownDate = toolsDate;
            }
            var message = $"Ферма отключилась {systemShutDownDate.Value} и включилась {systemUpTime}";
            _logger.Warn(message);
            await EventProcedures.SaveEvent(EventName.BadActivity, message);
        }
    }
}
