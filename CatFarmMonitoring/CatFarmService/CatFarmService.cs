﻿using System.Diagnostics;
using System.ServiceProcess;
using System.Threading.Tasks;
using NLog;

namespace CatFarmService
{
    public partial class CatFarmService : ServiceBase
    {
        public CatFarmService()
        {
            InitializeComponent();
        }

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        protected override void OnStart(string[] args)
        {
            Properties.Settings.Default.Reload();
            var mainWork = new MainWork();
            Task.Factory.StartNew(() =>mainWork.Start());
        }

        protected override void OnStop()
        {
            _logger.Warn("Сервис остановлен");
        }

        [Conditional("DEBUG")]
        public void DebugStartMe()
        {

            OnStart(new string[]{});

        }
    }
}
