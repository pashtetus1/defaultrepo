﻿using System.ServiceProcess;

namespace CatFarmService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new CatFarmService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
