﻿using System;
using System.Diagnostics;
using System.Globalization;
using Microsoft.Win32;

namespace CatFarmService.Helpers
{
    static class Tools
    {
        public const string InternalDataDir = "c:\\CatFarmService";
        public const string DbDir = InternalDataDir + "\\db";

        public static DateTime SystemUpTime()
        {
                using (var uptime = new PerformanceCounter("System", "System Up Time"))
                {
                    uptime.NextValue();       
                    return DateTime.Now - TimeSpan.FromSeconds(uptime.NextValue());
                }
        }

        public static DateTime GetLastSystemShutdown()
        {
            var sKey = @"System\CurrentControlSet\Control\Windows";
            var key = Registry.LocalMachine.OpenSubKey(sKey);

            var sValueName = "ShutdownTime";
            var val = (byte[])key.GetValue(sValueName);
            var valueAsLong = BitConverter.ToInt64(val, 0);
            return DateTime.FromFileTime(valueAsLong);
        }
        private static readonly TimeSpan _fiveSeconds = TimeSpan.FromSeconds(5);
        public static string ToActualFormat(this DateTime time)
        {
            var now = CatTime.Now;
            var updateTime = time.ToString(CultureInfo.InvariantCulture);
            if (now - time < _fiveSeconds)
            {
                updateTime = "только что";
            }
            return updateTime;
        }
        //public static Stream GetDataStream()
        //{

        //}
    }
}
