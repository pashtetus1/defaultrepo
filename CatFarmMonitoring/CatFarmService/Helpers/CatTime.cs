﻿using System;

namespace CatFarmService.Helpers
{
    static class CatTime
    {
        public static readonly TimeProvider CurrentTimeProvider = new CurrentTimeReal();

        public static DateTime TimeStart = DateTime.Now;
        public static TimeSpan TimeFromStart => Now - TimeStart;
        public static DateTime Now => CurrentTimeProvider.Now;

    }

    internal abstract class TimeProvider
    {
        public abstract DateTime Now
        { get; }
    }

    class CurrentTimeReal : TimeProvider
    {
        public override DateTime Now => DateTime.Now;
    }

    //class CurrentTimeConcrete : CurrentTimeProvider
    //{
    //    protected CurrentTimeConcrete()
    //    {
    //    }

    //    public override DateTime Now => DateTime.Now;
    //}

}
