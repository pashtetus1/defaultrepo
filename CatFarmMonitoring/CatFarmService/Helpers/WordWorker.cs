﻿using CatFarmService.Models.Events;
using CatFarmService.Models.States;

namespace CatFarmService.Helpers
{
    static class WordWorker
    {
        enum Gender
        {
            Men,
            Women
        }
        public static string GenerateMessage(HistoryChangeState notifyHistoryRecord)
        {
            var messageBegin = "Объект";
            var gender = Gender.Women;
            switch (notifyHistoryRecord.StateId)
            {
                //case StateName.ComputerWork:
                //    messageBegin = "Ферма";
                //    break;
                case StateName.Temperature:
                    messageBegin = "Температура фермы";
                    break;
                case StateName.InternetConnection:
                    messageBegin = "Интернет";
                    gender = Gender.Men;
                    break;
            }

            var oldStateString = GetWord(notifyHistoryRecord.OldStatus);
            var newStateString = GetWord(notifyHistoryRecord.NewStatus);

            var was = "была";
            var hasBecome = "стала";
            switch (gender)
            {
              
                case Gender.Men:
                    was = "был";
                    hasBecome = "стал";
                    break;
            }

            return $"{messageBegin} {was} {oldStateString} {hasBecome} {newStateString} в {notifyHistoryRecord.DateTime:HH:mm:ss}. {notifyHistoryRecord.LinkedData}";
        }

        public static string GetWord(string rawWord)
        {
            switch (rawWord)
            {
                case StateWords.Connect:
                    return "подключен";
                case StateWords.Disconnect:
                    return "разорван";
                case StateWords.On:
                    return "включена";
                case StateWords.Off:
                    return "выключена";
                case StateWords.Low:
                    return "низкая";
                case StateWords.Normal:
                    return "нормальная";
                case StateWords.Hight:
                    return "высокая";
                case StateWords.VideoAccident:
                    return "двоякая";
                default:
                    return "не знаю";
            }
        }

        public static string GenerateMessage(ServiceEvent serviceEvent)
        {
            var startWords = "Что-то случилось";
            switch (serviceEvent.EventId)
            {
                case EventName.FatalError:
                    startWords = "Фатальная ошибка";
                    break;
                case EventName.Error:
                    startWords = "Ошибка";
                    break;
                case EventName.BadActivity:
                    startWords = "Не желательное поведение";
                    break;
                case EventName.Expected:
                    startWords = "Ожидаемое событие";
                    break;
            }
            return $"{startWords}: {serviceEvent.Text}";

        }
    }
}
