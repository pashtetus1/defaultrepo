﻿using System.Threading.Tasks;
using CatFarmService.Models.Variables;
using NLog;

namespace CatFarmService.Helpers
{
    static class FarmCommander
    {

        public static async Task Restart()
        {
            
            await VariableProcedures.SetVariable(CatVariables.LastForceRestart, CatTime.Now);
            var logger = LogManager.GetCurrentClassLogger();
            logger.Info("Начало перезагрузки");
            await Task.Factory.StartNew(async () =>
            {
                await Task.Delay(5000);
                logger.Info("Щас перезагружусь");
                return System.Diagnostics.Process.Start("shutdown", "/r /t 0");
            });
        }
    }
}
