﻿namespace CatFarmService.Models.Data
{
    static class DataWords
    {
        public const string GpuTemperature = "GPU Core Temperature";
        public const string GpuFan = "GPU Fan Control";
        public const string GpuMemoryLoad = "GPU Memory Load";
        public const string GpuCoreLoad = "GPU Core Load";
        public const string GpuMemoryControllerLoad = "GPU Memory Controller Load";
    }
}
