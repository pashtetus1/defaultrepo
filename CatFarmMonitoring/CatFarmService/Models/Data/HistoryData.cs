﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CatFarmService.Models.Data
{
    class HistoryData
    {
        [Key]
        public int Id { get; set; }

        public DateTime DateTime { get; set; }

        public string DeviceName { get; set; }

        public string Data { get; set; }

    }
}
