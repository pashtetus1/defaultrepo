﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using CatFarmService.Helpers;
using CatFarmService.Models.States;
using Newtonsoft.Json.Linq;

namespace CatFarmService.Models.Data
{
    static class DataProcedures
    {
        public static async Task<string> GetCurrentStates()
        {
            List<CurrentData> devices;
            using (var db = new CatFarmDb())
            {
                devices = await db.CurrentData.ToListAsync();
            }
            var states = "Cтатус видеокарт:";
           
        
            foreach (var device in devices)
            {

                var updateTime = device.DateTime.ToActualFormat();
                states += $"\r\n{device.DeviceName} последнее обновление {updateTime}:\r\n";
                var json = JToken.Parse(device.Data);
                var temperature = json.Value<double?>(DataWords.GpuTemperature);
                var fan = json.Value<double?>(DataWords.GpuFan);
                var coreLoad = json.Value<double?>(DataWords.GpuCoreLoad);
                var memoryLoad = json.Value<double?>(DataWords.GpuMemoryLoad);
                var memoryControllerLoad = json.Value<double?>(DataWords.GpuMemoryControllerLoad);
                if (temperature != null)
                {
                    states += $"Темп-ра {temperature:##.#}° ";
                }
                if (fan != null)
                {
                    states += $"Кулер {fan:##.#}%\r\n";
                }
                states += $"Загрузки: ";
                if (coreLoad != null)
                {
                    states += $"ядро {coreLoad:##.#}% ";
                }
                if (memoryLoad != null)
                {
                    states += $"память {memoryLoad:##.#}%\r\n";
                }
                if (memoryControllerLoad != null)
                {
                    states += $"контроллер памяти {memoryControllerLoad:##.#}%";
                }
            }
            var tempState = await StateProcedures.GetCurrentState(StateName.Temperature);
            if (tempState != StateWords.Normal)
            {
                var wordState = WordWorker.GetWord(tempState);
                states += $"\r\nТемпература {wordState} !!!";
            }
            return states;
        }

       
    }
}
