﻿using System;
using System.Data.Entity;
using System.Globalization;
using System.Threading.Tasks;

namespace CatFarmService.Models.Variables
{
    static class VariableProcedures
    {
        public static async Task SetVariable(CatVariables id, long value)
        {
            await SetVariable(id, value.ToString());
        }

        public static async Task SetVariable(CatVariables id, DateTime value)
        {
            await SetVariable(id, value.ToString(CultureInfo.InvariantCulture));
        }

        public static async Task SetVariable(CatVariables id, string value)
        {
            using (var db = new CatFarmDb())
            {
                var variable = await db.Variables.FirstOrDefaultAsync(x => x.Id == id);
                if (variable != null)
                {
                    variable.Value = value;
                    variable.UpdateDateTime = DateTime.Now;
                }
                else
                {
                    variable = new Variable
                    {
                        Id = id,
                        UpdateDateTime = DateTime.Now,
                        Value = value
                    };
                    db.Variables.Add(variable);
                }
                await db.SaveChangesAsync();
            }
        }

        public static async Task<string> GetVariable(CatVariables id)
        {
            using (var db = new CatFarmDb())
            {
                var variable = await db.Variables.FirstOrDefaultAsync(x => x.Id == id);
                return variable?.Value;
            }
        }

        public static async Task<long> GetLongVariable(CatVariables id)
        {
            var result = await GetVariable(id);
            if (result == null)
            {
                return 0;
            }
            return long.Parse(result);
        }

        public static async Task<DateTime?> GetDateTimeVariable(CatVariables id)
        {
            var result = await GetVariable(id);
            if (result == null)
            {
                return null;
            }
            return DateTime.Parse(result, CultureInfo.InvariantCulture);
        }
    }
}
