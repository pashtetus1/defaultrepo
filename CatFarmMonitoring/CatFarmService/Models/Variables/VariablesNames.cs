﻿namespace CatFarmService.Models.Variables
{
    enum CatVariables
    {
        ComputerStart,
        MaxMemoryUsage,
        LastWorkDate,
        LastForceRestart
    }
}
