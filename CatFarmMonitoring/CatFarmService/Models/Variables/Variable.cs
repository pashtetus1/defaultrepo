﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CatFarmService.Models.Variables
{
    class Variable
    {
        [Key]
        public CatVariables Id { get; set; }

        public DateTime UpdateDateTime { get; set; }

        public string Value { get; set; }

        public string Name
        {
            get => Id.ToString("g");
            set { }
        }
    }
}

