﻿using System;
using System.Data.Entity;
using System.IO;
using CatFarmService.Helpers;
using NLog;

namespace CatFarmService.Models
{
    static class DbRecreator
    {
        public static void SafetyCreateNewBase(this DbContext context, string fileDb)
        {
            try
            {
                context.Database.Initialize(false);
            }
            catch (Exception e)
            {
                TryToCreateNewBase(context, e, fileDb);
            }
        }

        private static void TryToCreateNewBase(DbContext context, Exception e, string dbName)
        {
            var connectionString = context.Database.Connection.ConnectionString;
            var filePath = $"{Tools.DbDir}\\{dbName}";

            var logger = LogManager.GetCurrentClassLogger();
            if (connectionString.Contains(filePath))
            {
                logger.Error(e, $"не получилось проинициализировать базу {filePath}, попытка создать новую");
                var dirName = Path.GetDirectoryName(filePath);
                var fileName = Path.GetFileNameWithoutExtension(filePath);
                var ext = Path.GetExtension(filePath);
                var newfilePath = $"{dirName}\\moldy_{fileName}_{DateTime.Now:yyyy_MMM_dd__HH_mm}{ext}";
                if (File.Exists(newfilePath))
                {
                    logger.Info($"База по этому пути уже есть {newfilePath} добавим секунд");
                    newfilePath = $"{dirName}\\moldy_{fileName}_{DateTime.Now:yyyy_MMM_dd__HH_mm_ss}{ext}";
                }
                File.Copy(filePath, newfilePath);
                File.Delete(filePath);
                context.Database.Initialize(false);
                logger.Info($"Успешная повторная попытка создания базы {fileName}");
            }
            else
            {
                logger.Fatal(e, $"не получилось проинициализировать базу {dbName}");
            }
        }
    }
}
