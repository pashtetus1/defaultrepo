﻿using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.SQLite.EF6.Migrations;

namespace CatFarmService.Models.NlogDb
{
    class NLogDb : DbContext
    {
        static NLogDb()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<NLogDb, ContextMigrationConfiguration>(true));
        }

        public static void Initialize()
        {
            using (var context = new NLogDb())
            {
                context.SafetyCreateNewBase("nlog.db");
            }
        }

        public DbSet<LogEntry> LogEntries { get; set; }
    }

    internal sealed class ContextMigrationConfiguration : DbMigrationsConfiguration<NLogDb>
    {
        public ContextMigrationConfiguration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            SetSqlGenerator("System.Data.SQLite", new SQLiteMigrationSqlGenerator());

        }
    }
}
