﻿using System;
using System.ComponentModel.DataAnnotations;

// ReSharper disable UnusedMember.Global
// ReSharper disable ClassNeverInstantiated.Global

namespace CatFarmService.Models.NlogDb
{
    internal class LogEntry
    {
        [Key]
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Message { get; set; }
        public string Level { get; set; }
        public string Logger { get; set; }
        public string ExceptionInfo { get; set; }
    }
}
