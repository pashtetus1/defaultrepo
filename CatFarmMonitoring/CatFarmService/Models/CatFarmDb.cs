﻿using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.SQLite.EF6.Migrations;
using CatFarmService.Models.Data;
using CatFarmService.Models.DefferedAlertProccesing;
using CatFarmService.Models.Events;
using CatFarmService.Models.States;
using CatFarmService.Models.Telegram;
using CatFarmService.Models.Variables;

namespace CatFarmService.Models
{
    class CatFarmDb : DbContext
    {

       // private readonly static Logger Logger = LogManager.GetCurrentClassLogger();
        static CatFarmDb()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<CatFarmDb, ContextMigrationConfiguration>(true));
        }

      

        public DbSet<CurrentData> CurrentData { get; set; }
       
        public DbSet<HistoryData> HistoryData { get; set; }

        public DbSet<TelegramUser> TelegramUsers { get; set; }

        public DbSet<TelegramLinkId> TelegramLinks { get; set; }

        public DbSet<UserNotification> UserNotifications { get; set; }
        
        public DbSet<CurrentState> CurrentStates { get; set; }

        public DbSet<HistoryChangeState> HistoryStates { get; set; }

        public DbSet<Variable> Variables { get; set; }

        public DbSet<ServiceEvent> Events { get; set; }

        public DbSet<DefferedAlert> DefferedAlerts { get; set; }

        public static void Initialize()
        {
            using (var context = new CatFarmDb())
            {
                context.SafetyCreateNewBase("catfarm.db");
            }
        }

       
    }

    internal sealed class ContextMigrationConfiguration : DbMigrationsConfiguration<CatFarmDb>
    {
        public ContextMigrationConfiguration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            SetSqlGenerator("System.Data.SQLite", new SQLiteMigrationSqlGenerator());

        }
    }
}
