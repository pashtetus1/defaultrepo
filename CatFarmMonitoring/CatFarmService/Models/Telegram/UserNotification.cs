﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CatFarmService.Models.Telegram
{
    class UserNotification
    {
        [Key]
        public int Id { get; set; }

        public string Message { get; set; }

        public bool RequireSend { get; set; }

        public DateTime? SendDate { get; set; }

        public TelegramUser User { get; set; }

        public bool Loud { get; set; }

        public UserNotification()
        {
            Loud = true;
        }
    }
}
