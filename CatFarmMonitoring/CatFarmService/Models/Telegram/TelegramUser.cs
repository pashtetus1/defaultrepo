﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
// ReSharper disable UnusedMember.Global

namespace CatFarmService.Models.Telegram
{
    class TelegramUser
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long ChatId { get; set; }

        public DateTime RegDate { get; set; }
      
        public bool SubscribeActive { get; set; }

        public string UserName { get; set; }

        public UserRights Right { get; set; }

        public TelegramLinkId LinkKeyId { get; set; }

    }
}
