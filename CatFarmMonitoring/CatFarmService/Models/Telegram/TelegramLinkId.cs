﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CatFarmService.Models.Telegram
{
    class TelegramLinkId
    {
        [Key]
        public Guid Id { get; set; }

        public DateTime CreateDate { get; set; }

        public bool Active { get; set; }

        public DateTime ExpiredDate { get; set; }

       // public Guid LinkKeyId { get; set; }
    }
}
