﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using NLog;

namespace CatFarmService.Models.Telegram
{
    static class TelegramProcedures
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        //public static async Task PrepareStateChangesMassNotify(int historyEventId)
        //{
        //    _logger.Debug("Массовая рассылка изменений состояний");
        //    using (var db = new CatFarmDb())
        //    {
        //        foreach (var user in db.TelegramUsers.Where(x=> x.SubscribeActive))
        //        {
        //            var historyEvent = await db.HistoryStates.FirstOrDefaultAsync(x => x.Id == historyEventId);
        //            var newNotifications = new UserNotification
        //            {
        //                HistoryRecord = historyEvent,
        //                RequireSend = true,
        //                User = user
        //            };
        //            db.UserNotifications.Add(newNotifications);
        //        }
        //        await db.SaveChangesAsync();
        //    }
        //}

        public static async Task PrepareEventMassNotify(string message, bool loud = true)
        {
            //Logger.Debug("Массовая рассылка событий");
            using (var db = new CatFarmDb())
            {
                db.PrepareEventMassNotify(message, loud);
                await db.SaveChangesAsync();
            }
        }

        public static void PrepareEventMassNotify(this CatFarmDb db, string message, bool loud )
        {
            foreach (var user in db.TelegramUsers.Where(x => x.SubscribeActive))
            {
                // var Event = await db.Events.FirstOrDefaultAsync(x => x.Id == eventId);
                var newNotifications = new UserNotification
                {
                    Message = message,
                    RequireSend = true,
                    User = user,
                    Loud = loud
                };
                db.UserNotifications.Add(newNotifications);
            }
        }

        public enum Result
        {
            Already,
            СhangeSuccessfully
        }
        public static async Task<Result> ActivateUser(long chatId)
        {
            using (var db = new CatFarmDb())
            {
                var user = await db.TelegramUsers.FirstOrDefaultAsync(x => x.ChatId == chatId);
                if (user.SubscribeActive)
                {
                    return Result.Already;
                }
                user.SubscribeActive = true;
                await db.SaveChangesAsync();
            }
            return Result.СhangeSuccessfully;
        }

        public static async Task<Result> DeactivateUser(long chatId)
        {
            Logger.Debug("Хочу деактивировать чувака");
            using (var db = new CatFarmDb())
            {
               
                var user = await db.TelegramUsers.FirstOrDefaultAsync(x => x.ChatId == chatId);
                if (!user.SubscribeActive)
                {
                    return Result.Already;
                }
                user.SubscribeActive = false;
                //if (db.Database.CurrentTransaction != null)
                //{
                   
                //}
                //await db.SaveChangesAsync();
                var notifications = db.UserNotifications.Where(x => x.RequireSend && x.User.ChatId == chatId);
                foreach (var notification in notifications)
                {
                    notification.RequireSend = false;
                }
                await db.SaveChangesAsync();
            }
            return Result.СhangeSuccessfully;
        }

        public static async Task<bool> IsNoUsers()
        {
            using (var mainDb = new CatFarmDb())
            {
                return !await mainDb.TelegramUsers.AnyAsync();
            }

           
        }

        public static async Task SetReservedUsers()
        {
            using (var mainDb = new CatFarmDb())
            {
                var userExistInMainBase = await mainDb.TelegramUsers.AnyAsync();
                if (userExistInMainBase)
                {
                    using (var reserveDb = new ReserveDb())
                    {
                        if (await TableEquals(reserveDb.TelegramUsers, mainDb.TelegramUsers))
                        {
                            return;
                        }
                        reserveDb.TelegramUsers.AddRange(mainDb.TelegramUsers);
                        await reserveDb.SaveChangesAsync();
                        Logger.Warn("Резервная база пользователей дополнена");
                    }
                    return;
                }

                using (var reserveDb = new ReserveDb())
                {
                    var reserveUserExist = await reserveDb.TelegramUsers.AnyAsync();
                    if (reserveUserExist)
                    {
                        mainDb.TelegramUsers.AddRange(reserveDb.TelegramUsers);
                        await mainDb.SaveChangesAsync();
                        Logger.Info("Пользователи взяты из резервной базы");
                        return;
                    }

                }
            }
            Logger.Warn("Пользователей не нашлось ни в резервной ни в основной базе");
        }

        private static async Task<bool> TableEquals(IQueryable<TelegramUser> reserveDbTelegramUsers, IQueryable<TelegramUser> mainDbTelegramUsers)
        {
            if (reserveDbTelegramUsers.Count() != mainDbTelegramUsers.Count())
            {
                return false;
            }

            foreach (var user in mainDbTelegramUsers)
            {
                var userExist = await reserveDbTelegramUsers.AnyAsync(x => x.ChatId == user.ChatId);
                if (!userExist)
                {
                    return false;
                }
            }
            return true;
        }
        public enum RegisterResult
        {
            Succes,
            AlreadyRegister,
            Fail
        }
        public static async Task<RegisterResult> TryToRegisterNewUser(long chatId, Guid id, string userName)
        {
            using (var db = new CatFarmDb())
            {
                TelegramLinkId registeredLink = null;
                foreach (var link in db.TelegramLinks)
                {
                    if (link.Id == id)
                    {
                        registeredLink = link;
                    }
                }

                //  var chat = message.Chat;
                if (registeredLink != null)
                {
                    RegisterResult result;
                    if (registeredLink.Active && registeredLink.ExpiredDate >= DateTime.Now)
                    {
                        var user = db.TelegramUsers.FirstOrDefault(x => x.ChatId == chatId);
                        if (user == null)
                        {
                            var newUser = new TelegramUser
                            {
                                ChatId = chatId,
                                LinkKeyId = registeredLink,
                                RegDate = DateTime.Now,
                                SubscribeActive = true,
                                UserName = userName
                            };
                            db.TelegramUsers.Add(newUser);
                            using (var reserveDb = new ReserveDb())
                            {
                                reserveDb.TelegramUsers.Add(newUser);
                                await reserveDb.SaveChangesAsync();
                            }
                            Logger.Warn($"Новая регистрация айди: {chatId}  имя: {userName}");
                            registeredLink.Active = false;
                            result = RegisterResult.Succes;
                        }
                        else
                        {
                            //  user.SubscribeActive = true;
                            Logger.Warn($"Чувак пытается зарегаться два раза: {chatId}  имя: {userName}");
                            result = RegisterResult.AlreadyRegister;
                        }
                    }
                    else
                    {
                        registeredLink.Active = false;
                        result = RegisterResult.Fail;
                    }

                    await db.SaveChangesAsync();
                    return result;
                }
                else
                {
                    return RegisterResult.Fail;
                    //proval
                }
            }
        }
    }
}
