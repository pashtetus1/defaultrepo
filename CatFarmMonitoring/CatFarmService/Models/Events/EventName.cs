﻿namespace CatFarmService.Models.Events
{
    enum EventName
    {
        FatalError,
        Error,
        BadActivity,
        Expected
    }
}
