﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CatFarmService.Models.Events
{
    class ServiceEvent
    {
        [Key]
        public int Id { get; set; }

        public EventName EventId { get; set; }
       
        public DateTime FixDateTime { get; set; }

        public string EventName
        {
            get => EventId.ToString("g");
            set { }
        }

        public string Text { get; set; }

        public string LinkedData { get; set; }

    }
}
