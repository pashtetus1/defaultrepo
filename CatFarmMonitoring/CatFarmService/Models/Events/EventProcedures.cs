﻿using System;
using System.Threading.Tasks;
using CatFarmService.Helpers;
using CatFarmService.Models.Telegram;
using NLog;

namespace CatFarmService.Models.Events
{
    static class EventProcedures
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static async Task SaveEvent(EventName stateName, string text,string linkedData)
        {
            await SaveEvent(stateName, text, null, linkedData);
        }

        public static async Task SaveEvent(EventName stateName, string text, DateTime? eventDate = null, string linkedData = null)
        {
            if (eventDate == null)
            {
                eventDate = DateTime.Now;
            }
            var newEvent = new ServiceEvent
            {
                EventId = stateName,
                FixDateTime = eventDate.Value,
                Text = text,
                LinkedData = linkedData
            };

            using (var db = new CatFarmDb())
            {
                db.Events.Add(newEvent);
                await db.SaveChangesAsync();
            }

            Logger.Debug($"Произошло событие {newEvent.EventName} {newEvent.Text}. Фиксация события {eventDate.Value.ToActualFormat()} ");

            var message = WordWorker.GenerateMessage(newEvent);

            var loud = stateName != EventName.Expected;
            await TelegramProcedures.PrepareEventMassNotify(message, loud);
        }
    }
}
