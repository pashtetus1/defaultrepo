﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using CatFarmService.Helpers;
using CatFarmService.Models.DefferedAlertProccesing;
using CatFarmService.Models.Telegram;
using CatFarmService.Models.Variables;
using NLog;

namespace CatFarmService.Models.States
{
    static class StateProcedures
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static async Task SaveChangeStateAsync(StateName stateName, string newStatus, string linkedData = null)
        {
           
            var dateNow = CatTime.Now;
            var oldState = "oldState";
            using (var db = new CatFarmDb())
            {
                var stateRecord = await db.CurrentStates.FirstOrDefaultAsync(x => x.Id == stateName);
                if (stateRecord == null)
                {
                    stateRecord = new CurrentState()
                    {
                        Id = stateName,
                        UpdateDateTime = dateNow,
                        StateStatus = newStatus,
                        LinkedData = linkedData
                    };
                    db.CurrentStates.Add(stateRecord);
                    await db.SaveChangesAsync();
                    Logger.Warn($"Создано новое состояние {stateName:g} текущее состояние {newStatus}");
                    return;
                }
                oldState = stateRecord.StateStatus;
                if (oldState == newStatus)
                {
                    return;
                }
                stateRecord.StateStatus = newStatus;
                stateRecord.UpdateDateTime = dateNow;
                stateRecord.LinkedData = linkedData;
                await db.SaveChangesAsync();
              
            }
            await SaveHistoryEvent(stateName,oldState,newStatus, linkedData);
        }

        private static async Task SaveHistoryEvent(StateName stateName, string oldStatus, string newStatus, string linkedData = null)
        {
            var dateNow = CatTime.Now;
            var newHistoryChange = new HistoryChangeState
            {
                StateId = stateName,
                DateTime = dateNow,
                OldStatus = oldStatus,
                NewStatus = newStatus,
                LinkedData = linkedData
            };
            using (var db = new CatFarmDb())
            {
                db.HistoryStates.Add(newHistoryChange);
                await db.SaveChangesAsync();
            }
            Logger.Debug($"Произошло изменение статуса {stateName:g} текущее состояние {newStatus} старое {oldStatus}");


            using (var db = new CatFarmDb())
            {
                var cancDefAlerts = await db.DefferedAlerts.Where(x=> x.CurrentAlertState == AlertState.Wait).Include(x=> x.LinkedChangeState).FirstOrDefaultAsync(x=> x.LinkedChangeState.StateId == stateName);
                if (cancDefAlerts != null)
                {
                    cancDefAlerts.CurrentAlertState = AlertState.Cancel;
                    await db.SaveChangesAsync();
                    Logger.Debug($"Отложенное состояние {cancDefAlerts.FarmStateId:G} отменено");
                    if (cancDefAlerts.LinkedChangeState.OldStatus == newStatus)
                    {
                        Logger.Debug($"{cancDefAlerts.FarmStateId:G} успело вернуться в {newStatus}");
                        return;
                    }
                }
            }
            var computerStartDate = await VariableProcedures.GetDateTimeVariable(CatVariables.ComputerStart);
            if ((dateNow - computerStartDate.Value).Minutes < 5)
            {
                if (stateName == StateName.InternetConnection || stateName == StateName.Temperature)
                {
               
                    if ((dateNow - computerStartDate.Value).Minutes < 5)
                    {
                        await DeferEvent(newHistoryChange, TimeSpan.FromMinutes(5));
                        return;
                    }
                }
            }

            if (stateName == StateName.InternetConnection && newStatus == StateWords.Disconnect)
            {
                await DeferEvent(newHistoryChange, TimeSpan.FromSeconds(50));
                return;
            }

            if (stateName == StateName.Temperature && newStatus == StateWords.Low)
            {
                await DeferEvent(newHistoryChange, TimeSpan.FromMinutes(4));
                return;
            }

            if (stateName == StateName.Temperature && newStatus == StateWords.Hight)
            {
                await DeferEvent(newHistoryChange, TimeSpan.FromMinutes(10));
                return;
            }

            if (stateName == StateName.Temperature && newStatus == StateWords.Normal)
            {
                await DeferEvent(newHistoryChange, TimeSpan.FromMinutes(10));
                return;
            }

            var loud = IsLoud(newStatus);
            var message = WordWorker.GenerateMessage(newHistoryChange);
            await TelegramProcedures.PrepareEventMassNotify(message, loud);
        }

        public static bool IsLoud(string newStatus)
        {
            switch (newStatus)
            {
                case StateWords.Connect:
                case StateWords.On:
                case StateWords.Normal:
                    return false;
            }
            return true;
        }

        private static async Task DeferEvent(HistoryChangeState newHistoryChange, TimeSpan timeToWait)
        {
            using (var db = new CatFarmDb())
            {
                var newDefferedAlert =
                    new DefferedAlert
                    {
                        ApprovalDate = newHistoryChange.DateTime.Add(timeToWait),
                        CurrentAlertState = AlertState.Wait,
                        LinkedChangeState = newHistoryChange
                    };
                db.DefferedAlerts.Add(newDefferedAlert);
                await db.SaveChangesAsync();
                Logger.Debug($"Отложенное состояние {newHistoryChange.StateId:G} возможное состояние {newHistoryChange.NewStatus}");
            }
        }

        public static async Task<string> GetCurrentState(StateName stateName)
        {
            using (var db = new CatFarmDb())
            {
                var connections = await db.CurrentStates.FirstOrDefaultAsync(x=> x.Id == stateName);
                return connections?.StateStatus;
            }
        }
    }
}
