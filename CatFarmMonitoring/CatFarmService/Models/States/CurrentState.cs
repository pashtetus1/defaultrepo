﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CatFarmService.Models.States
{
    class CurrentState
    {
        [Key]
        public StateName Id { get; set; }

        public DateTime UpdateDateTime { get; set; }

        public string StateStatus { get; set; }

        public string StateName
        {
            get => Id.ToString("g");
            set {  }
        }

        public string LinkedData { get; set; }

    }
}
