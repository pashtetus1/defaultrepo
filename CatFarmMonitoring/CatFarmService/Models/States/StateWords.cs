﻿namespace CatFarmService.Models.States
{
    class StateWords
    {
        public const string On = "On";
        public const string Off = "Off";
        public const string Connect = "Connect";
        public const string Disconnect = "Disconnect";
        public const string Hight = "Hight";
        public const string VideoAccident = "VideoAccident";
        public const string Low = "Low";
        public const string Normal = "Normal";
    }
}
