﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CatFarmService.Models.States
{
    class HistoryChangeState
    {
        [Key]
        public int Id { get; set; }

        public StateName StateId { get; set; }

        public DateTime DateTime { get; set; }

        public string NewStatus { get; set; }
        public string OldStatus { get; set; }

        public string LinkedData { get; set; }

    }
}
