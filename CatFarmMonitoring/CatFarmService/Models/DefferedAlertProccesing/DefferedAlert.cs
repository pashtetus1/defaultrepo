﻿using System;
using System.ComponentModel.DataAnnotations;
using CatFarmService.Models.States;

namespace CatFarmService.Models.DefferedAlertProccesing
{
    class DefferedAlert
    {
        [Key]
        public int Id { get; set; }
       
        public HistoryChangeState LinkedChangeState { get; set; }

        public DateTime ApprovalDate { get; set; }

        public AlertState CurrentAlertState { get; set; }
        public StateName FarmStateId => LinkedChangeState.StateId;
    }
}
