﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using CatFarmService.Helpers;
using CatFarmService.Models.States;
using CatFarmService.Models.Telegram;
using NLog;

namespace CatFarmService.Models.DefferedAlertProccesing
{
    static class DefAlertProcedures
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public static async Task DefferedAlertCheck()
        {
            var now = CatTime.Now;
            using (var db = new CatFarmDb())
            {
                var activatingAlerts =
                    db.DefferedAlerts.Where(x => x.CurrentAlertState == AlertState.Wait && x.ApprovalDate < now).Include(x=> x.LinkedChangeState);

                foreach (var alert in activatingAlerts)
                {
                    var changeState = alert.LinkedChangeState;
                    var loud = StateProcedures.IsLoud(changeState.NewStatus);
                    var message = WordWorker.GenerateMessage(changeState);
                    message += $" Это событие было отложено до {alert.ApprovalDate.ToShortTimeString()}";
                    db.PrepareEventMassNotify(message, loud);
                    alert.CurrentAlertState = AlertState.Approval;
                    Logger.Debug($"Отложенное состояние {changeState.StateId:G} всё таки установилось, новое состояние {changeState.NewStatus}");
                  
                }
                await db.SaveChangesAsync();
            }
        }
    }
}
