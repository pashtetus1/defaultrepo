﻿using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.SQLite.EF6.Migrations;
using CatFarmService.Models.Telegram;

namespace CatFarmService.Models
{
    class ReserveDb : DbContext
    {
        static ReserveDb()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ReserveDb, ContextMigrationConfigurationReserv>(true));
        }
        public static void Initialize()
        {
            using (var context = new ReserveDb())
            {
                context.SafetyCreateNewBase("reserve.db");
            }
        }

        public DbSet<TelegramUser> TelegramUsers { get; set; }
    }

    internal sealed class ContextMigrationConfigurationReserv : DbMigrationsConfiguration<ReserveDb>
    {
        public ContextMigrationConfigurationReserv()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            SetSqlGenerator("System.Data.SQLite", new SQLiteMigrationSqlGenerator());

        }
    }

}
