﻿using System;
using System.Threading.Tasks;
using CatFarmService.Models.Events;
using NLog;

namespace CatFarmService.Works
{
    abstract class BaseSafetyWork
    {
        protected int RestartTimeInMiliseconds;
        protected readonly Logger Logger;

        protected BaseSafetyWork()
        {
            Logger = LogManager.GetLogger(GetType().Name);
            RestartTimeInMiliseconds = 1000;
        }

        protected virtual Task<bool> IsExpected(Exception e)
        {
            var taskSource = new TaskCompletionSource<bool>();
            taskSource.SetResult(false);
            return taskSource.Task;
        }
        public async Task Start()
        {
            while (true)
            {
                try
                {
                    await OneOffStartWork();
                    Logger.Info($"Запущена работа {GetType().Name}");
                    break;
                }
                catch (Exception e)
                {
                    if (await IsExpected(e))
                    {
                        await Task.Delay(RestartTimeInMiliseconds*5);
                        continue;
                    }
                    await UnknownError(e);
                    return;
                 
                }
               

            }

            while (true)
            {
                try
                {
                    await PeriodicallyWork();
                    await Task.Delay(RestartTimeInMiliseconds);
                }
                catch (Exception e)
                {
                    if (await IsExpected(e))
                    {
                        await Task.Delay(RestartTimeInMiliseconds*5);
                        continue;
                    }
                    await UnknownError(e);
                    return;
                }

            }
        }

        private async Task UnknownError(Exception e)
        {
            var message = $"Неизвестная ошибка при работе {GetType().Name}. Задача свернута.";
            Logger.Fatal(e, message);
            await EventProcedures.SaveEvent(EventName.FatalError, message, e.ToString());
        }

     //   protected abstract Task<bool> IsExpected(Exception e);
        protected abstract Task PeriodicallyWork();
        protected abstract Task OneOffStartWork();
    }
}
