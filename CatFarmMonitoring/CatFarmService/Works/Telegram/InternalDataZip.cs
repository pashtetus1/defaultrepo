﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using CatFarmService.Helpers;
namespace CatFarmService.Works.Telegram
{
    class InternalDataZip : IDisposable
    {
        private readonly MemoryStream _memoryStream = new MemoryStream();
        
        public InternalDataZip()
        {
            var files = DirSearch(Tools.InternalDataDir);
            using (var zipArchive = new ZipArchive(_memoryStream, ZipArchiveMode.Create, true))
            {
                foreach (var attachment in files)
                {
                    try
                    {
                        zipArchive.CreateEntryFromFile(attachment, Path.GetFileName(attachment),
                            CompressionLevel.Optimal);
                    }
                    catch (IOException)
                    {
                        const string tempPath = "temp";
                        if (File.Exists(tempPath))
                        {
                            File.Delete(tempPath);
                        }
                        File.Copy(attachment, tempPath);
                        zipArchive.CreateEntryFromFile(tempPath, Path.GetFileName(attachment),
                            CompressionLevel.Optimal);
                        File.Delete(tempPath);
                    }

                }
            }

         


        }

        public Stream ZipStream => new MemoryStream(_memoryStream.ToArray());

        public void Dispose()
        {
            _memoryStream.Dispose();
        }

        private static List<String> DirSearch(string sDir)
        {
            List<String> files = new List<String>();

            foreach (string f in Directory.GetFiles(sDir))
            {
                files.Add(f);
            }
            foreach (string d in Directory.GetDirectories(sDir))
            {
                files.AddRange(DirSearch(d));
            }


            return files;
        }
    }
}
