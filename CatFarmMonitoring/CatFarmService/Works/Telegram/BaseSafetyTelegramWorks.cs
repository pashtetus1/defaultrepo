﻿using System;
using System.Data.Entity;
using System.Net.Http;
using System.Threading.Tasks;
using CatFarmService.Models;
using CatFarmService.Models.States;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types.Enums;

namespace CatFarmService.Works.Telegram
{
    abstract class BaseSafetyTelegramWorks: BaseSafetyWork
    {
       // public bool InternetConnectionWatchSeverety = true;
      
        protected TelegramBotClient Bot { get; } = new TelegramBotClient(Properties.Settings.Default.TelegramToken);
        protected static bool IsConnected { get; private set; } = true;

        protected sealed override async Task OneOffStartWork()
        {
            using (var db = new CatFarmDb())
            {
                var internetConnection = await db.CurrentStates.FirstOrDefaultAsync(x => x.Id == StateName.InternetConnection);
                if (internetConnection == null)
                {
                    Logger.Info($"Записи о подключении не оказалось в базе, будем считать что интернет есть");
                    IsConnected = true;
                    await StateProcedures.SaveChangeStateAsync(StateName.InternetConnection, StateWords.Connect);
                    await db.SaveChangesAsync();
                }
                else switch (internetConnection.StateStatus)
                {
                    case StateWords.Connect:
                        IsConnected = true;
                        break;
                    default:
                        IsConnected = false;
                        break;
                }
            }
            await OneOffTelegramStartWork();
        }
        protected sealed override async Task<bool> IsExpected(Exception e)
        {
            if (e is ApiRequestException || e is HttpRequestException)
            {
                await IsDisconnectedEvent(e);
                return true;
            }
            if (e is JsonReaderException)
            {
                var message = "Ошибка json при работе телеграмма";
                Logger.Error(e, message);
                //await EventProcedures.SaveEvent(EventName.Error, message);
                //await Task.Delay(5000);
                return true;
            }
            
            return false;
        }
        protected async Task IsConnectedEvent()
        {
            if (IsConnected) return;
         
            IsConnected = true;
            await StateProcedures.SaveChangeStateAsync(StateName.InternetConnection, StateWords.Connect);
            Logger.Info("Соединение восстановилось");
        }

        protected async Task SendMessage(long chatId, string message, bool loud = true)
        {
                await Bot.SendTextMessageAsync(chatId, message, ParseMode.Default, false, !loud);
            
           
        }

        private async Task IsDisconnectedEvent(Exception e)
        {
            if (!IsConnected) return;
            IsConnected = false;
            var apiRequestException = e as ApiRequestException;
            await StateProcedures.SaveChangeStateAsync(StateName.InternetConnection, StateWords.Disconnect);
            Logger.Error(e, $"Разрыв соединения {apiRequestException?.ErrorCode}, попытка перезапуститься");
        }
       
        protected abstract Task OneOffTelegramStartWork();
        // protected abstract Task OneOffStartWork();

    }
}
