﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CatFarmService.Helpers;
using CatFarmService.Models;
using CatFarmService.Models.Data;
using CatFarmService.Models.Telegram;
using Telegram.Bot.Types;
using Message = Telegram.Bot.Types.Message;

#pragma warning disable 4014

namespace CatFarmService.Works.Telegram
{
    internal class TelegramBotWorker : BaseSafetyTelegramWorks
    {
        private const int DefaultLifeTimeInSec = 3000;
        private const int GuidLength = 36;
        private const string StartCommand = "/start";
        private const string GenerateNewInviteCommand = "/newlink";
        private const string GetInternalData = "/getlogs";
        private const string StopSubscriptionCommand = "/stop";
        private const string ShowStateCommand = "/state";
        private const string RestartCommand = "/restartfarm";
        private const string NoAccess = "нет доступа";
        private string _botName;
       

        private int _lastUpdateId = 0;


       
        protected override async Task OneOffTelegramStartWork()
        {
            _botName = (await Bot.GetMeAsync()).Username;
            Logger.Debug($"Telegram bot name get: {_botName}");

            await GenerateFirstLink();
            Logger.Debug($"{GetType()} run");
        }

        protected override async Task PeriodicallyWork()
        {
            var updates = await Bot.GetUpdatesAsync(_lastUpdateId, 100, 1);

            foreach (var update in updates)
            {
                var message = update.Message;
                if (message.EntityValues.Contains(StartCommand) && message.Text.Length > GuidLength)
                {
                    await TryToRegisterNewUser(message);
                }
                else
                {
                    if (Verification(message.Chat.Id))
                    {
                        await ProccessCommand(message);
                    }
                    else
                    {
                        await Bot.SendTextMessageAsync(message.Chat.Id, NoAccess);
                        Logger.Warn($"Кто-то бьется в бота без авторизации. айди: {message.Chat.Id} сообщение: {message.Text}");
                    }

                }
                _lastUpdateId = update.Id;
            }


            if (updates.Length != 0)
            {
                _lastUpdateId++;
            }
            await IsConnectedEvent();

        }


       


        private async Task GenerateFirstLink()
        {
            if (await TelegramProcedures.IsNoUsers())
            {
                Logger.Debug($"GenerateFirstLink run");
                var link = await GenerateNewLink(DefaultLifeTimeInSec*10);
                Logger.Warn($"Создана новая ссылка для регистрации \"{link}\"");
                ThrowLinkInBrowser(link);
            }
        }

        [Conditional("DEBUG")]
        private void ThrowLinkInBrowser(string link)
        {
            try
            {
                Process.Start($"{link}");
            }
            catch (Exception e)
            {
                Logger.Error(e, $"не удалось чтото сделать с ссылкой {link}");
            }
        }

        private async Task<string> GenerateNewLink(int lifeTime = DefaultLifeTimeInSec)
        {
            using (var db = new CatFarmDb())
            {
                var botName = _botName;

                var now = DateTime.Now;
                var guid = Guid.NewGuid();
                var newLinkId = new TelegramLinkId
                {
                    Active = true,
                    CreateDate = now,
                    ExpiredDate = now.AddSeconds(lifeTime),
                    Id = guid
                };
                db.TelegramLinks.Add(newLinkId);
                var link = $"https://telegram.me/{botName}?start={guid}";
                await db.SaveChangesAsync();
                return link;

            }

        }

        


     
        private async Task TryToRegisterNewUser(Message message)
        {
            Guid id;
            try
            {
                var stringId = message.Text.Substring(StartCommand.Length + 1, GuidLength);
                id = new Guid(stringId);
            }
            catch (Exception e)
            {
               Logger.Error($"{e} сообщение {message.Text}");
               return;
            }
            var chat = message.Chat;
            var result = await TelegramProcedures.TryToRegisterNewUser(chat.Id, id, chat.Username);

            switch (result)
            {
                case TelegramProcedures.RegisterResult.Succes:
                    await SendMessage(chat.Id, $"Вы зарегистрированы {chat.Username}");
                    break;
                case TelegramProcedures.RegisterResult.AlreadyRegister:
                    await SendMessage(chat.Id, $"Вы уже были зарегистрированы {chat.Username}");
                    break;
                case TelegramProcedures.RegisterResult.Fail:
                    Logger.Warn($"Провал регистрации: {chat.Id} сообщение: {message.Text}");
                    await SendMessage(chat.Id, NoAccess);
                    break;
             
            }
        }

     

        private async Task ProccessCommand(Message message)
        {
            var expectedMessage = true;
            var chatId = message.Chat.Id;
            if (message.EntityValues.Contains(GenerateNewInviteCommand))
            {
                var newLink = await GenerateNewLink();
                await Bot.SendTextMessageAsync(chatId, $"Cсылка готова \r\n{newLink}");
            }
            else if (message.EntityValues.Contains(ShowStateCommand))
            {
                var states = await DataProcedures.GetCurrentStates();
                await Bot.SendTextMessageAsync(chatId, states);
            }
            else if (message.EntityValues.Contains(StartCommand))
            {
                var result = await TelegramProcedures.ActivateUser(chatId);

                var wordMessage = "Все рассылки успешно включены";
                if (result == TelegramProcedures.Result.Already)
                {
                    wordMessage = "Все рассылки уже были включены";
                }
                await Bot.SendTextMessageAsync(chatId, wordMessage);
            }
            else if (message.EntityValues.Contains(StopSubscriptionCommand))
            {
                var result = await TelegramProcedures.DeactivateUser(chatId);

                var wordMessage = "Все рассылки успешно отключены";
                if (result == TelegramProcedures.Result.Already)
                {
                    wordMessage = "Все рассылки уже были отключены";
                }
                await Bot.SendTextMessageAsync(chatId, wordMessage);
            }
            else if (message.EntityValues.Contains(GetInternalData))
            {
                await Task.Factory.StartNew(() =>SendZipData(chatId));
                Logger.Debug("Пошел дальше");
            }
            else if (message.EntityValues.Contains(RestartCommand))
            {
                await FarmCommander.Restart();
                await SendMessage(chatId, "Скоро перезагружусь", false);
            }
            else
            {
                expectedMessage = false;
                await Bot.SendTextMessageAsync(message.Chat.Id, "Не понимаю");
                Logger.Info($"Не известная команда от: {chatId} сообщение: {message.Text}");
            }
            if (expectedMessage)
            {
                Logger.Info($"Ожидаемая команда от: {chatId} сообщение: {message.Text}");
            }
        }

        private async Task SendZipData(long chatId)
        {
            using (var zip = new InternalDataZip())
            {
                var file = new FileToSend("data.zip", zip.ZipStream);
                await Bot.SendDocumentAsync(chatId, file, "Архив с внутренними логами и данными");
            }
            Logger.Debug("Кончил отправлять");
        }

        private bool Verification(long chatId)
        {
            using (var db = new CatFarmDb())
            {
                return db.TelegramUsers.Any(x=> x.ChatId == chatId);
            }
        }
    }
}
