﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using CatFarmService.Models;
using CatFarmService.Models.Telegram;
using Telegram.Bot.Exceptions;

namespace CatFarmService.Works.Telegram
{
    class TelegramNotificationSender : BaseSafetyTelegramWorks
    {
       // private readonly Logger _logger = LogManager.GetCurrentClassLogger();
       
        protected override async Task PeriodicallyWork()
        {
            if (!IsConnected)
            {
                return;
            }
            var badChats = new List<long>();
            using (var db = new CatFarmDb())
            {
                var possibleNotifications = db.UserNotifications.Where(x => x.RequireSend).
                    Include(x => x.User);
                var userIds = possibleNotifications.Select(x => x.User.ChatId).Distinct();
                foreach (var userId in userIds)
                {
                    await ProccessUserMessages(possibleNotifications, userId, badChats);
                }

                if (possibleNotifications.Any())
                {
                    await IsConnectedEvent();
                }
               
                await db.SaveChangesAsync();
            }
            foreach (var chatId in badChats)
            {
                Logger.Info($"Чат {chatId} заблокировался, его подписка будет приостановлена");
                await TelegramProcedures.DeactivateUser(chatId);
            }
        
        }

        private async Task ProccessUserMessages(IQueryable<UserNotification> possibleNotifications, long userId, List<long> badChats)
        {
            var userNotifications = possibleNotifications.Where(x => x.User.ChatId == userId);

            try
            {
                await SendEventNotification(userNotifications);
            }
            catch (ApiRequestException e)
            {
                if (e.ErrorCode == 403)
                {
                    badChats.Add(userId);
                }
                else
                {
                    throw;
                }
            }
        }

        private async Task SendEventNotification(IEnumerable<UserNotification> stateChangesNotification)
        {
            foreach (var notify in stateChangesNotification)
            {
                await SendMessage(notify.User.ChatId, notify.Message, notify.Loud);

                notify.RequireSend = false;
                notify.SendDate = DateTime.Now;
            }
        }

        protected override Task OneOffTelegramStartWork()
        {
            return Task.CompletedTask;
        }
    }
}
