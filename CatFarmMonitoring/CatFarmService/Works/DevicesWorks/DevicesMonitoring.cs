﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using CatFarmService.Models;
using CatFarmService.Models.Data;
using CatFarmService.Models.States;

namespace CatFarmService.Works.DevicesWorks
{
    class DevicesMonitoring : BaseDevicesWork
    {

        public DevicesMonitoring()
        {
            RestartTimeInMiliseconds = 3000;
        }
       

        protected override async Task PeriodicallyWork()
        {
            var devices = GetDevicesData(MyComputer);
            //Logger.Debug($"Девайсов {devices.Count}");
            await FixStatesChange(devices);

            await FixHistory(devices);
        }

        private async Task FixStatesChange(List<DeviceData> devices)
        {
            var maxTemperature = double.MinValue;
            var minTemperature = double.MaxValue;

            foreach (var device in devices)
            {
                var temperature = device.Data.Value<double>(DataWords.GpuTemperature);

                if (maxTemperature < temperature)
                {
                    maxTemperature = temperature;
                }
                if (minTemperature > temperature)
                {
                    minTemperature = temperature;
                }
            }

            var hotTemp = Properties.Settings.Default.HightTemperature;
            var coldTemp = Properties.Settings.Default.LowTemperature;
            //var accidents = 0;
            var hightTemp = false;
            var lowTemp = false;
            if (maxTemperature > hotTemp)
            {
                hightTemp = true;
            }

            if (minTemperature < coldTemp)
            {
                lowTemp = true;
            }
            //if (CatTime.TimeFromStart < TimeSpan.FromSeconds(3))
            //{
            //    lowTemp = false;
            //}
            //else if (CatTime.TimeFromStart < TimeSpan.FromSeconds(200))
            //{
            //    lowTemp = true;
            //}
            if (hightTemp && lowTemp)
            {
                var message = "Имеет место двойная авария одна карта слишком горячая дургая слишком холодная";
                Logger.Error(message);
                await StateProcedures.SaveChangeStateAsync(StateName.Temperature, StateWords.VideoAccident,
                    $"Текущая минимальная температура {minTemperature}° максимальная {maxTemperature}°");
            }
            else if (hightTemp)
            {
                await StateProcedures.SaveChangeStateAsync(StateName.Temperature, StateWords.Hight,
                    $"Текущая максимальная температура {maxTemperature}°");
            }
            else if(lowTemp)
            {
                await StateProcedures.SaveChangeStateAsync(StateName.Temperature, StateWords.Low,
                    $"Текущая минимальная температура {minTemperature}°");
            }
            else if (minTemperature > coldTemp + 5 && maxTemperature < hotTemp - 5)
            {
                await StateProcedures.SaveChangeStateAsync(StateName.Temperature, StateWords.Normal);
            }
        }

        private static async Task FixHistory(List<DeviceData> devices)
        {
            using (var db = new CatFarmDb())
            {
                var now = DateTime.Now;
                foreach (var device in devices)
                {
                    var deviceData = await db.CurrentData.FirstOrDefaultAsync(x => x.DeviceName == device.Name);
                    if (deviceData == null)
                    {
                        deviceData = new CurrentData
                        {
                            DateTime = now,
                            DeviceName = device.Name,
                            Data = device.Data.ToString()
                        };
                        db.CurrentData.Add(deviceData);
                    }
                    else
                    {
                        deviceData.DateTime = now;
                        deviceData.Data = device.Data.ToString();
                    }
                }


                await db.SaveChangesAsync();
            }
        }
    }
}
