﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using OpenHardwareMonitor.Hardware;

namespace CatFarmService.Works.DevicesWorks
{
    abstract class BaseDevicesWork : BaseSafetyWork
    {

        protected static readonly Computer MyComputer = new Computer
        {
            GPUEnabled = true
        };

        private static bool IsOpen = false;
        protected override Task OneOffStartWork()
        {
            if (IsOpen) return Task.CompletedTask;
            lock (MyComputer)
            {
                if (IsOpen) return Task.CompletedTask;
                IsOpen = true;
                Logger.Debug("Создается и открывается новый компьютер");
                MyComputer.Open();
            }
          
           
            return Task.CompletedTask;
        }

        protected static List<DeviceData> GetDevicesData(Computer myComputer)
        {
            var datas = new List<DeviceData>();
            var gpuIndex = 0;
            foreach (var hardware in myComputer.Hardware)
            {
                var deviceData = HardwareGetData(hardware, gpuIndex);
                datas.Add(deviceData);
                gpuIndex++;
            }
            return datas;
        }

        private static DeviceData HardwareGetData(IHardware hardware, int gpuIndex)
        {
            hardware.Update();

            var dic = new Dictionary<string, double>();
            foreach (var sensor in hardware.Sensors)
            {
                dic.Add($"{sensor.Name} {sensor.SensorType}", sensor.Value.GetValueOrDefault());
            }
            var deviceName = $"{hardware.Name} №{gpuIndex}";
            var data = JToken.FromObject(dic);
            return new DeviceData
            {
                Name = deviceName,
                Data = data
            };

        }
    }
}
