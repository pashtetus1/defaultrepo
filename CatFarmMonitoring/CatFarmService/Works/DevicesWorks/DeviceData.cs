﻿using Newtonsoft.Json.Linq;

namespace CatFarmService.Works.DevicesWorks
{
    struct DeviceData
    {
        public string Name { get; set; }
        public JToken Data { get; set; }

    }
}