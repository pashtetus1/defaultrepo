﻿using System;
using System.Threading.Tasks;
using CatFarmService.Models;
using CatFarmService.Models.Data;

namespace CatFarmService.Works.DevicesWorks
{
    class DeviceHistoryWriting : BaseDevicesWork
    {
        public DeviceHistoryWriting()
        {
            SetHistoryInterval();
        }

        private void SetHistoryInterval()
        {
            RestartTimeInMiliseconds = Properties.Settings.Default.HistoryWritingIntervalSec * 1000;
        }

        protected override async Task PeriodicallyWork()
        {
            var devices = GetDevicesData(MyComputer);
            using (var db = new CatFarmDb())
            {
                var now = DateTime.Now;
                foreach (var device in devices)
                {
                    var deviceData = new HistoryData
                    {
                        DateTime = now,
                        DeviceName = device.Name,
                        Data = device.Data.ToString()
                    };
                    db.HistoryData.Add(deviceData);
                }

                await db.SaveChangesAsync();
            }

            SetHistoryInterval();

        }
    }
}
