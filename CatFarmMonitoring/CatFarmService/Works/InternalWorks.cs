﻿using System.Diagnostics;
using System.Threading.Tasks;
using CatFarmService.Models.Events;
using CatFarmService.Models.Variables;

namespace CatFarmService.Works
{
    class InternalWorks : BaseSafetyWork
    {
        private readonly Process _currentProc = Process.GetCurrentProcess();

        public InternalWorks()
        {
            RestartTimeInMiliseconds = 30000;
        }

        protected override async Task PeriodicallyWork()
        {
            var memory = ToMb(_currentProc.PrivateMemorySize64);
            var lastMemory = await VariableProcedures.GetLongVariable(CatVariables.MaxMemoryUsage);

            if (memory > lastMemory)
            {
                await VariableProcedures.SetVariable(CatVariables.MaxMemoryUsage, memory);
                if (lastMemory > 70)
                {
                    var message = $"Использование памяти повысилось до {memory} mb";
                    Logger.Warn(message);
                    await EventProcedures.SaveEvent(EventName.BadActivity, message);
                }
            }
            Properties.Settings.Default.Reload();
        }

        protected override Task OneOffStartWork()
        {
            return Task.CompletedTask;
        }


        private static long ToMb(long bytes)
        {
            return bytes / 1000000;
        }
    }
}
