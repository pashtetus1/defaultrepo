﻿using System;
using System.Threading.Tasks;
using CatFarmService.Models.Variables;

namespace CatFarmService.Works
{
    class WorkTimeWriter : BaseSafetyWork
    {
        protected override async Task PeriodicallyWork()
        {
            await VariableProcedures.SetVariable(CatVariables.LastWorkDate, DateTime.Now);
        }

        protected override Task OneOffStartWork()
        {
            RestartTimeInMiliseconds = 3000;
            return Task.CompletedTask;
        }
    }
}
