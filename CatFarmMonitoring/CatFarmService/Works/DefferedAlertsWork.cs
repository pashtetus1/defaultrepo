﻿using System.Threading.Tasks;
using CatFarmService.Models.DefferedAlertProccesing;

namespace CatFarmService.Works
{
    class DefferedAlertsWork: BaseSafetyWork
    {
        protected override async Task PeriodicallyWork()
        {
            await DefAlertProcedures.DefferedAlertCheck();
        }

        protected override Task OneOffStartWork()
        {
            RestartTimeInMiliseconds = 3000;
            return Task.CompletedTask;
        }
    }
}
