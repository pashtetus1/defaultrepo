﻿using System;

namespace ServiceDebugStarter
{
    class Program
    {
        static void Main(string[] args)
        {
            var service = new CatFarmService.CatFarmService();
            service.DebugStartMe();
            Console.ReadLine();
        }
    }
}
